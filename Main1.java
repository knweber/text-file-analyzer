
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*;

import javax.swing.JFileChooser;

import java.io.*;

public class Main extends JPanel {
	private Vector projectList;
	private JButton browse;
	private JTextArea tarea=new JTextArea();
	private JTextField lines = new JTextField();
	private JTextField avgCharPerLine = new JTextField();
	private JTextField spaces = new JTextField();
	private JTextField blankLines = new JTextField();
	private JTextField words = new JTextField();
	private JPanel input = new JPanel();
	private JLabel filename=new JLabel();
	//private ProjectSpendingPanel spendingPanel;

	// Constructor initializes components and organize them using certain
	// layouts
	public Main() {
		//this.projectList = projectList;
		//this.spendingPanel = spendingPanel;
		browse = new JButton("Browse");
		//browse.setSize(new Dimension(5,10));
		
		setLayout(new GridLayout(1, 2));
		//Create labels text feilds and panels
		
		JPanel data = new JPanel(new GridLayout(8, 2));
		//JTextField lines = new JTextField();
		
		
		
		JTextField avgWordLength = new JTextField();
		JTextField mostCommonWord = new JTextField();
		
		
		
		
		
		//place items in proper locations
		input.add(new JLabel("Input (a) Text File(s) to be analyzed"));
		input.add(browse);
		input.add(filename);
		input.add(tarea);
		data.add(new JLabel("Lines:"));
		data.add(lines);
		data.add(new JLabel("Blank Lines:"));
		data.add(blankLines);
		data.add(new JLabel("Spaces:"));
		data.add(spaces);
		data.add(new JLabel("Words:"));
		data.add(words);
		data.add(new JLabel("Averages"));
		data.add(new JLabel());
		data.add(new JLabel("Characters per line:"));
		data.add(avgCharPerLine);
		data.add(new JLabel("Word Length:"));
		data.add(avgWordLength);
		data.add(new JLabel("Most Common Words:"));
		data.add(mostCommonWord);
		
		add(input);
		add(data);
		//setLayout(new GroupLayout());
		
		//listen for button press and execute code
		ButtonListener click = new ButtonListener();
		browse.addActionListener(click);
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			int countl=0,countch=0,countsp=0,countbl=0,countw=0,j=0;
			String string1 = "",string2="";
			String[] word;
			tarea.setText("");
			lines.setText("");
			avgCharPerLine.setText("");
			spaces.setText("");
			blankLines.setText("");
			words.setText("");
			filename.setText("");
			
			
			if (event.getSource() == browse){
				//string = String.format("field 1: %s", event.getActionCommand());
				JFileChooser open=new JFileChooser();
				int show=open.showOpenDialog(null);
				if(show==JFileChooser.APPROVE_OPTION)
				{
					File f=open.getSelectedFile();
			        
					String fname=f.getName();
					filename.setText(fname);
					String extension=fname.substring(fname.lastIndexOf('.')+1);
					
					if(!(extension.equals("txt")))   //check for the validity of the file
					{
						JOptionPane.showMessageDialog(input,"Please try a text file.","Invalid file type",JOptionPane.ERROR_MESSAGE);
					}
					else
					{
					String filepath=f.getPath();
					try
					{
						BufferedReader br=new BufferedReader(new FileReader(filepath));
						while((string1=br.readLine())!=null)
						{
							string2=string2+string1+"\n";
							countl++;                                    //count of total lines including blank lines
							if(string1.length()==0)
							{
								countbl++;                              //count of blank lines
							}
							word=string1.split(" ");
							countw=countw+word.length;                //count of words
							int i=0;
							while(i<string1.length())
							{
								if((string1.charAt(i))!=' ')
								{
									countch++;            //not counting the spaces for average characters per line
								}
								else
								{
									countsp++;            //count of spaces
								}
								i++;
							}
						}

						tarea.setText(string2);
						lines.setText(String.valueOf(countl));
						avgCharPerLine.setText(String.valueOf(countch/countl));
						spaces.setText(String.valueOf(countsp));
						blankLines.setText(String.valueOf(countbl));
						words.setText(String.valueOf(countw-countbl));
						br.close();
						
					}
					catch(Exception ex)
					{
						System.out.println(ex);
					}
				}
				
				}
				}
			}
		} // end of actionPerformed method
	} // end of ButtonListener class

