import java.awt.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.*;

public class History extends JPanel {
	
	private ArrayList<String> listData = new ArrayList<String>();
	private JList<String> list;
	private JScrollPane scroll;
	private JTextField avgCharPerLine = new JTextField();
	private JTextField avgWordLength = new JTextField();
	private JTextField lines = new JTextField();
	private JTextField totalavgCharPerLine = new JTextField();
	private JTextField totalavgWordLength = new JTextField();
	private JTextField totallines = new JTextField();
	private static final long serialVersionUID = 1L;
	
	public History(ArrayList<String> listData, final ArrayList<String[]> hist){
		this.listData = listData;
		
		setLayout(new GridLayout(1,2));
		
		//populate the UI with components
		JPanel right = new JPanel(new GridLayout(2,1));
		JPanel avg = new JPanel(new GridLayout(4,2));
		JPanel totalavg = new JPanel(new GridLayout(4,2));
		scroll = new JScrollPane(list = new JList<String>(listData.toArray(new String[listData.size()])));
		avg.add(new JLabel("Selected Averages"));
		avg.add(new JLabel(""));
		avg.add(new JLabel("Characters per line:"));
		avg.add(avgCharPerLine);
		avg.add(new JLabel("Word Length:"));
		avg.add(avgWordLength);
		avg.add(new JLabel("Lines:"));
		avg.add(lines);
		totalavg.add(new JLabel("Total Averages"));
		totalavg.add(new JLabel());
		totalavg.add(new JLabel("Characters per line:"));
		totalavg.add(totalavgCharPerLine);
		totalavg.add(new JLabel("Word Length:"));
		totalavg.add(totalavgWordLength);
		totalavg.add(new JLabel("Lines:"));
		totalavg.add(totallines);
		//make the JTextField read-only
		lines.setEditable(false);
		avgCharPerLine.setEditable(false);
		avgWordLength.setEditable(false);
		totallines.setEditable(false);
		totalavgCharPerLine.setEditable(false);
		totalavgWordLength.setEditable(false);
		
		right.add(avg);
		right.add(totalavg);
		add(scroll);
		add(right);
		
		//check for selection activity on the jlist and fill in the data
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (!event.getValueIsAdjusting()) {
					//update the averages based on the selected JLists element
					int index = list.getSelectedIndex();
					String[] temp = hist.get(index);
					avgCharPerLine.setText(temp[0]);
					avgWordLength.setText(temp[1]);
					lines.setText(temp[2]);
				}
			}
		});
	}
	
	//with new data imported refresh the jlist and empty the previous average data
	public void update() {
		//update the data in the JList 
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(int i = 0; i < listData.size(); i++)
			model.addElement(listData.get(i));
		list.setModel(model);
		repaint();
	}
	
	// to set the average characters per line across multiple files
	public void setavgcharperline(String str)
	{
		totalavgCharPerLine.setText(str);
	}
	
	// to set the average word length across multiple files
	public void setavgwordlength(String str)
	{
		totalavgWordLength.setText(str);
	}
	
	//to set the average number of lines across multiple files
	public void setavglines(String str)
	{
		totallines.setText(str);
	}
}