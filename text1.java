import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class text1 {
	int countl=0,countch=0,countsp=0,countbl = 0,countw=0,j=0;
	String string1 = "",string2="";
	String[] word;
	Boolean found = false;
	ArrayList<String> dictionary = new ArrayList<String>();
	ArrayList<int[]> duplicates = new ArrayList<int[]>();
	int[] temp = new int[2];


	@Test
	public void test() {
		String filepath = "C:\\Users\\parla\\Desktop\\501_assign.txt";
		try
		{
		BufferedReader bl = new  BufferedReader(new FileReader(filepath));
		while((string1=bl.readLine())!=null)
		{
			string2=string2+string1+"\n";
			countl++;
			if(string1.length()==0)
			{
				countbl++;                              //count of blank lines
			}
			//Change all words to lower case
			word = string1.split("\\P{Alpha}+");
			for(int i = 0; i < word.length; i++) {
				word[i] = word[i].toLowerCase();
			}
			
			//Insert words to dictionary for analysis
			for (int i = 0;i < word.length; i++) {
				//word not found in dictionary
				if(!dictionary.contains(word[i])){
					dictionary.add(word[i]);
				//word in dictionary
				}else{
					//increment number of times a word is shown
					for(int k = 0; k < duplicates.size(); k++) {
						temp = duplicates.get(k);
						//case of found duplicate
						if(temp[0] == dictionary.indexOf(word[i])) {
							found = true;
							temp[1]++;
							duplicates.set(k, temp);
							k = duplicates.size();
						}
					}
					temp = new int[2];
					//case of first or new duplicate
					temp[0] = dictionary.indexOf(word[i]);
					temp[1] = 1;
					if(!found) {
						duplicates.add(temp);
					}
					found = false;
				}
			}
			
			countw = countw + word.length;                //count of words
			int i = 0;
			while(i < string1.length())
			{
				if((string1.charAt(i))!=' ')
				{
					countch++;            //not counting the spaces for average characters per line
				}
				else
				{
					countsp++;            //count of spaces
				}
				i++;
			}
		}

		//find the most times a word has shown up in the text
		int max = 0;
		String commonWords = "";
		for(int i = 0; i < duplicates.size(); i++) {
			temp = duplicates.get(i);
			if(temp[1] > max)
				max = temp[1];
		}

		//find the words with the max number of duplicates and append them to the common words string
		for(int i = 0; i < duplicates.size(); i++) {
			temp = duplicates.get(i);
			if(temp[1] == max)
				commonWords += dictionary.get(temp[0]) + " ";
		}
	      
		
		assertEquals(52,countl);
		assertEquals(0,countbl);
		assertEquals(409,countsp);
		assertEquals(413,countw);
		assertEquals(35,countch/countl);
		assertEquals(4,countch/countw);
		String str = commonWords;
		assertEquals("the ",str);
		
		bl.close();
  }
		catch(Exception ex)
		{
			System.out.println(ex);
		}	
	}


}
