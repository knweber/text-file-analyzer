import java.util.*;

import javax.swing.*;

public class TextFileAnalyzer {
	// declare private variables
	private static JTabbedPane tPane;
	private static Main main;
	private static History history;
	private static ArrayList<String> listData = new ArrayList<String>();
	private static ArrayList<String[]> hist = new ArrayList<String[]>();
	private static void GUI() {
		// create windows to be inserted into tabs

		history = new History(listData, hist);
		main = new Main(listData, hist, history);

		// create a frame and ensure it can be closed
		JFrame frame = new JFrame("Text Analyzer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// populate the frame with a tabbed pane containing each window
		tPane = new JTabbedPane();
		tPane.addTab("Main", main);
		tPane.addTab("History", history);
		tPane.addTab("Help",
				new JTextArea("To select a file, Select browse under the main tab.\n"
						+ "Upon file selection, Data will populate the appropriate text feilds.\n\n"
						+ "To review data from previous submitions, naivate to the History tab and select a text file from the list.\n"
						+ "Along side the history data, the cumulative averages will be displayed."));
		frame.getContentPane().add(tPane);

		// set frame variables
		frame.setSize(650, 450);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		GUI();
	}
}