import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.JFileChooser;
import javax.swing.border.EmptyBorder;

import java.io.*;

public class Main extends JPanel {
	// create variables for storing text file data
	private JButton browse;
	private JTextArea tarea = new JTextArea();
	private JTextArea tareaPunc = new JTextArea();
	private JTextField lines = new JTextField();
	private JTextField avgCharPerLine = new JTextField();
	private JTextField avgWordLength = new JTextField();
	private JTextField spaces = new JTextField();
	private JTextField blankLines = new JTextField();
	private JTextField words = new JTextField();
	private JPanel input = new JPanel();
	private JLabel filename = new JLabel();
	private JTextField mostCommonWord = new JTextField();
	public static int favgCharPerLine=0;   //static variables to calculate and pass average values across multiple files
	public static int flines=0;
	public static int countfiles=0;
	public static int fspaces=0;
	public static int fblankLines=0;
	public static int fwords=0;
	public static int favgWordLength=0;
	private static final long serialVersionUID = 1L;
	private History history;
	public ArrayList<String> listData = new ArrayList<String>();
	public ArrayList<String[]> hist = new ArrayList<String[]>();
	

	public Main(ArrayList<String> listData, ArrayList<String[]> hist, History history) {
		this.listData = listData;
		this.hist = hist;
		this.history = history;

		JPanel data = new JPanel(new GridLayout(8, 2));
		browse = new JButton("Browse");

		// set the layout
		data.setBorder(new EmptyBorder(5, 5, 5, 5));
		input.setBorder(new EmptyBorder(5, 5, 5, 5));
		input.setLayout(new BoxLayout(input, BoxLayout.Y_AXIS));
		setLayout(new GridLayout(2, 2));

		// place items in proper locations
		input.add(new JLabel("Input (a) Text File(s) to be analyzed"));
		input.add(browse);
		input.add(filename);
		data.add(new JLabel("Lines:"));
		data.add(lines);
		data.add(new JLabel("Blank Lines:"));
		data.add(blankLines);
		data.add(new JLabel("Spaces:"));
		data.add(spaces);
		data.add(new JLabel("Words:"));
		data.add(words);
		data.add(new JLabel("Averages"));
		data.add(new JLabel("------"));
		data.add(new JLabel("Characters per line:"));
		data.add(avgCharPerLine);
		data.add(new JLabel("Word Length:"));
		data.add(avgWordLength);
		data.add(new JLabel("Most Common Words:"));
		data.add(mostCommonWord);
		
		//make the JTextField and JTextArea read-only
		lines.setEditable(false);
		blankLines.setEditable(false);
		spaces.setEditable(false);
		words.setEditable(false);
		avgCharPerLine.setEditable(false);
		avgWordLength.setEditable(false);
		mostCommonWord.setEditable(false);
		tarea.setEditable(false);
		tareaPunc.setEditable(false);
		
		add(input);
		add(data);
		add(new JScrollPane(tarea));
		add(new JScrollPane(tareaPunc));

		// listen for button press and execute code
		ButtonListener click = new ButtonListener();
		browse.addActionListener(click);
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			int countl = 0, countch = 0, countsp = 0, countbl = 0, countw = 0;
			String string1 = "", string2 = "";
			String[] word;
			Boolean found = false;
			ArrayList<String> dictionary = new ArrayList<String>();
			ArrayList<int[]> duplicates = new ArrayList<int[]>();
			int[] temp = new int[2];
			String[] data = new String[3];
			boolean success = false;

			// reset all text fields
			tarea.setText("");
			tareaPunc.setText("");
			lines.setText("");
			avgCharPerLine.setText("");
			avgWordLength.setText("");
			spaces.setText("");
			blankLines.setText("");
			words.setText("");
			filename.setText("");
			mostCommonWord.setText("");

			// browse button has been pressed
			if (event.getSource() == browse) {
				// string = String.format("field 1: %s",
				// event.getActionCommand());
				JFileChooser open = new JFileChooser();
				int show = open.showOpenDialog(null);
				if (show == JFileChooser.APPROVE_OPTION) {
					File f = open.getSelectedFile();

					String fname = f.getName();
					filename.setText(fname);
					String extension = fname.substring(fname.lastIndexOf('.') + 1);

					if (!(extension.equals("txt"))) // check for the validity of
													// the file
					{
						JOptionPane.showMessageDialog(input, "Please try a text file.", "Invalid file type",
								JOptionPane.ERROR_MESSAGE);
					} else {
						String filepath = f.getPath();
						try {
							BufferedReader br = new BufferedReader(new FileReader(filepath));
				            
							countfiles++;      //maintains count of total files opened during the current session
							while ((string1 = br.readLine()) != null) {
								string2 = string2 + string1 + "\n";
								countl++; // count of total lines including
											// blank lines
								if (string1.length() == 0) {
									countbl++; // count of blank lines
								}
								// Change all words to lowercase
								word = string1.split("\\P{Alpha}+");
								for (int i = 0; i < word.length; i++) {
									word[i] = word[i].toLowerCase();
								}

								// Insert words to dictionary for analysis
								for (int i = 0; i < word.length; i++) {
									// word not found in dictionary
									if (!dictionary.contains(word[i])) {
										dictionary.add(word[i]);
										// word in dictionary
									} else {
										// incriment number of times a word is
										// shown
										for (int k = 0; k < duplicates.size(); k++) {
											temp = duplicates.get(k);
											// case of found duplicate
											if (temp[0] == dictionary.indexOf(word[i])) {
												found = true;
												temp[1]++;
												duplicates.set(k, temp);
												k = duplicates.size();
											}
										}
										temp = new int[2];
										// case of first or new duplicate
										temp[0] = dictionary.indexOf(word[i]);
										temp[1] = 1;
										if (!found) {
											duplicates.add(temp);
										}
										found = false;
									}
								}

								countw = countw + word.length; // count of words
								int i = 0;
								while (i < string1.length()) {
									if ((string1.charAt(i)) != ' ') {
										countch++; // not counting the spaces
													// for average characters
													// per line
									} else {
										countsp++; // count of spaces
									}
									i++;
								}
							}

							// find the most times a word has shown up in the
							// text
							int max = 0;
							String commonWords = "";
							for (int i = 0; i < duplicates.size(); i++) {
								temp = duplicates.get(i);
								if (temp[1] > max)
									max = temp[1];
							}

							// find the words with the max number of duplicates
							// and append them to the common words string
							for (int i = 0; i < duplicates.size(); i++) {
								temp = duplicates.get(i);
								if (temp[1] == max)
									commonWords += dictionary.get(temp[0]) + " ";
							}

							// share the data between the main and history classes
							Date date = new Date();
							String file = fname + " " + date;
							listData.add(file);
							data[0] = String.valueOf(countch / countl);
							data[1] = String.valueOf(countch / countw);
							data[2] = String.valueOf(countl);
							hist.add(data);

							// populate the UI with the new data
							tarea.setText(string2);
							lines.setText(String.valueOf(countl));
							flines=countl+flines;          //calculate average number of lines across multiple files
							avgCharPerLine.setText(String.valueOf(countch / countl));
							favgCharPerLine=(countch/countl)+favgCharPerLine; //calculate average characters per line across multiple files
							avgWordLength.setText(String.valueOf(countch / countw));
							favgWordLength=(countch/countw)+favgWordLength;   //calculate average word length across multiple files
							spaces.setText(String.valueOf(countsp));
							blankLines.setText(String.valueOf(countbl));
							words.setText(String.valueOf(countw - countbl));
							if(commonWords.equals(" "))
							{
								mostCommonWord.setText("All words appear only once!");
							}
							else
							{
							    mostCommonWord.setText(commonWords);
							}
							tareaPunc.setText(string2.replaceAll("\\p{Punct}", ""));

							success = true;
							
							//update values in history tab
							history.update();
							history.setavgcharperline(String.valueOf(favgCharPerLine/countfiles));
							history.setavgwordlength(String.valueOf(favgWordLength/countfiles));
							history.setavglines(String.valueOf(flines/countfiles));
							br.close();

						} catch (Exception ex) {
							if (!success)
								System.out.println(ex);
						}
					}

				}
			}
		}
	} // end of actionPerformed method
} // end of ButtonListener class